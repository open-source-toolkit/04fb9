# 基于SpringBoot+Vue实验预约系统201524（附源码+数据库）

## 项目简介

本项目是一个基于SpringBoot和Vue框架开发的实验预约系统，适用于高校或科研机构的实验室预约管理。系统分为学生用户、老师用户和管理员三个角色，每个角色拥有不同的功能权限。项目附带完整的源码和数据库文件，方便开发者进行二次开发和部署。

## 功能模块

### 学生用户
1. **个人信息**：查看和修改个人资料。
2. **我的预约**：查看和管理自己的实验室预约记录。
3. **我的课程**：查看与自己相关的课程信息。
4. **实验室预约**：提交实验室预约申请。
5. **阅读公告**：查看系统发布的公告信息。

### 老师用户
1. **个人信息**：查看和修改个人资料。
2. **我的预约**：查看和管理自己的实验室预约记录。
3. **实验室预约**：提交实验室预约申请。
4. **我的课堂**：查看和管理自己负责的课程信息。
5. **阅读公告**：查看系统发布的公告信息。

### 管理员
1. **发布公告**：发布和管理系统公告。
2. **预约管理**：审核和管理所有用户的实验室预约申请。
3. **实验室管理**：管理实验室的基本信息和可用时间段。
4. **用户管理**：管理学生和老师的用户信息。

## 技术栈

- **后端**：SpringBoot
- **前端**：Vue.js
- **数据库**：MySQL
- **其他**：MyBatis、JWT、Swagger等

## 快速开始

### 环境要求
- JDK 1.8 或更高版本
- Maven 3.x
- Node.js 12.x 或更高版本
- MySQL 5.7 或更高版本

### 安装步骤

1. **克隆仓库**
   ```bash
   git clone https://github.com/your-repo/lab-reservation-system.git
   ```

2. **导入数据库**
   - 将项目根目录下的`lab_reservation.sql`文件导入到MySQL数据库中。

3. **配置后端**
   - 修改`src/main/resources/application.yml`文件中的数据库连接信息。
   - 运行SpringBoot项目：
     ```bash
     mvn spring-boot:run
     ```

4. **配置前端**
   - 进入`frontend`目录：
     ```bash
     cd frontend
     ```
   - 安装依赖：
     ```bash
     npm install
     ```
   - 启动前端项目：
     ```bash
     npm run serve
     ```

5. **访问系统**
   - 打开浏览器，访问`http://localhost:8080`，即可进入实验预约系统。

## 贡献指南

欢迎大家提交Issue和Pull Request，共同完善这个项目。在提交代码之前，请确保遵循以下规范：

1. **代码风格**：遵循项目现有的代码风格。
2. **提交信息**：提交信息应简洁明了，描述清楚修改的内容。
3. **测试**：确保新增功能或修改不会影响现有功能。

## 许可证

本项目采用MIT许可证，详情请参阅`LICENSE`文件。

## 联系我们

如有任何问题或建议，请通过GitHub Issue或邮件联系我们。

---

感谢您使用本项目，希望它能为您的实验室管理带来便利！